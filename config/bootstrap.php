<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$isDevMode = true;
$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/yaml"), $isDevMode);

// database configuration parameters
$conn = array(
	'driver'   => 'pdo_pgsql',
    'dbname'   => 'osio',
    'user'     => 'osio',
    'password' => 'osio',
    'charset'  => 'utf8',
);

// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);

?>
